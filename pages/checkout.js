import {
  Box,
  Container,
  Grid,
  Divider,
  Stack,
  RadioGroup,
  FormControlLabel,
  Radio,
} from "@mui/material";
import {
  Button,
  Content,
  FormGroup,
  TextField,
  Title,
} from "../components/elements";
import PropTypes from "prop-types";
import { useForm, Controller } from "react-hook-form";
import Navbar from "../components/Navbar/Navbar";
import Cards from "../components/sidebar/elements/cards";
import { getCartItems } from "../connect/shopping";
import { connect } from "react-redux";
import { useEffect } from "react";
import CartItem from "../components/cartItem";
import { priceFormat } from "../util/priceFormatter";

import React from "react";
import { shipping_address } from "../connect/shipping";
import ShippingItem from "../components/shippingItem";
import { usePaystackPayment } from "react-paystack";
import { checkout } from "../connect/transactions";

export const Checkbox = (props) => {
  useEffect(() => {
    props.getShippingAddress();
    props.loadCartItem();
  }, []);

  function getTotal() {
    return props.cartList.results.reduce(function (a, b) {
      return a + b.product.price * b.quantity;
    }, 0);
  }

  const config = {
    reference: new Date().getTime(),
    email: props.user.email,
    amount: getTotal() * 100,
    publicKey: "pk_live_45ee06a67470f003a29bacea44b2117e90c0fec9",
  };

  const initializePayment = usePaystackPayment(config)

  // you can call this function anything
  const onSuccess = (reference, data) => {
    data.transaction_no = reference.trxref;
    props.createdOrder(data).then((resp) => {
      if (resp.success) {
        window.location.replace("/account/purchase_history")
      }
    })
    // Implementation for whatever you want to do with reference and after success call.
    console.log(reference, data);
  };

  // you can call this function anything
  const onClose = (e) => {
    // implementation for  whatever you want to do when the Paystack dialog closed.
    console.log("closed");
  };


  const {
    control: control2,
    reset,
    formState: { errors: errors2 },
    handleSubmit: handleSubmit2,
  } = useForm({
    defaultValues: {
      shipping: "",
      payment_method: "card_payment"
    },
  });

  const onSubmit = (data) => {
    if (data.payment_method === "card_payment") {
      initializePayment((ref) => onSuccess(ref, data), onClose)
    } else {
      props.createdOrder(data).then((resp) => {
        if (resp.success) {
          window.location.replace("/account/purchase_history")
        }
      })
    }
  };

  return (
    <div>
      <Navbar />
      <Container sx={{ marginTop: 10 }} fixed>
        <Box sx={{ flexGrow: 1 }}>
          <Grid container spacing={4}>
            <Grid item xs={12} md={8}>
              <Cards
                title={`Cart Information (${props.cartList.count} Items)`}
                empty={props.cartList.results == 0}
              >
                {props.cartList.results.map((element, i) => (
                  <CartItem key={i} data={element} />
                ))}
              </Cards>
            </Grid>
            <Grid item xs={12} md={4}>
              <Cards title="Select Shipping Address">
              <Controller
                  name="shipping"
                  control={control2}
                  rules={{
                    required: {
                      value: true,
                      message: "Please a Shipping is required",
                    },
                  }}
                  render={({ field }) => (
                    <FormGroup>
                      {props.address.results.map((element, index) => (
                        <RadioGroup key={index} {...field}>
                          <FormControlLabel
                            value={element.id}
                            defaultValue={element.is_default}
                            control={<Radio />}
                            label={`${element.first_name} ${element.last_name}`}
                          />
                          <Content sx={{ width: "50%" }}>
                            {`${element.address_line1} ${element.address_line2} ${element.region}`}
                          </Content>
                          <Divider />
                        </RadioGroup>
                      ))}

                      {errors2.shipping && (
                        <Content
                          sx={{
                            marginTop: "8px",
                            fontSize: "10px",
                            color: "red",
                          }}
                        >
                          {errors2.shipping.message}
                        </Content>
                      )}
                    </FormGroup>
                  )}
                />
              </Cards>
              <Box sx={{ marginTop: 1 }} />
              <Cards title="Select Payment Method">
              <Controller
                  name="payment_method"
                  control={control2}
                  rules={{
                    required: {
                      value: true,
                      message: "Please a Role is required",
                    },
                  }}
                  render={({ field }) => (
                    <FormGroup>
                      {[{type: "Card", name: "card_payment"}, {type: "DrugStoc Pay", name: "drugstoc_pay"}].map((element, index) => (
                        <RadioGroup key={index} {...field}>
                          <FormControlLabel
                            value={element.name}
                            defaultValue={element.is_default}
                            control={<Radio />}
                            label={`${element.type}`}
                          />
                        </RadioGroup>
                      ))}

                      {errors2.shipping && (
                        <Content
                          sx={{
                            marginTop: "8px",
                            fontSize: "10px",
                            color: "red",
                          }}
                        >
                          {errors2.role.message}
                        </Content>
                      )}
                    </FormGroup>
                  )}
                />
              </Cards>
              <Box sx={{ marginTop: 5 }} />
              <Cards title="Summary">
                <Stack
                  direction="row"
                  sx={{ marginTop: 2 }}
                  justifyContent="space-between"
                  alignItems="center"
                >
                  <Content>Subtotal</Content>
                  <Title variant="h6">{priceFormat(getTotal())}.00</Title>
                </Stack>
                <Content sx={{ fontSize: 12 }}>
                  Delivery fees not included yet.
                </Content>
                <Divider sx={{ marginTop: 2, marginBottom: 2 }} />
                <Button variant="contained" onClick={handleSubmit2(onSubmit)}>Continue to Payment</Button>
              </Cards>
            </Grid>
          </Grid>
        </Box>
      </Container>
    </div>
  );
};

Checkbox.propTypes = {};

const mapStateToProps = (state) => {
  return {
    cartList: state.shopping.cart,
    address: state.shipping.shipping,
    user: state.user.user,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    loadCartItem: () => dispatch(getCartItems()),
    getShippingAddress: () => dispatch(shipping_address()),
    createdOrder: payload => dispatch(checkout(payload))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Checkbox);

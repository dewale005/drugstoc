import { Add, Edit } from "@mui/icons-material";
import { Box, Button, Divider, Grid, MenuItem, Stack } from "@mui/material";
import { useEffect, useState } from "react";
import { Controller, useForm } from "react-hook-form";
import { connect } from "react-redux";
import { Modal } from "semantic-ui-react";
import {
  Content,
  FormGroup,
  Label,
  SelectField,
  TextField,
  Title,
} from "../../components/elements";
import { Delete } from "../../components/icons";
import AccountLayout from "../../components/Layouts/account";
import ShippingItem from "../../components/shippingItem";
import Cards from "../../components/sidebar/elements/cards";
import { add_address, shipping_address } from "../../connect/shipping";
import { business_location } from "../../data/categories";
import Connect from "../../util/connect";
import { useDataFetcher } from "../../util/hooks";
import { BASE_URL, getInitialProps } from "../../util/resolveerror";

function ShippingAddress(props) {
  const [open, handleOpen] = useState(false);
  const [loading, handleLoading] = useState(false);

  const {data, isLoading, isError} = useDataFetcher({path: "/shipping/shipping-address", refreshInterval: true})


  const {
    control: control2,
    reset,
    formState: { errors: errors2 },
    handleSubmit: handleSubmit2,
  } = useForm();

  const onSubmit = (data) => {
    data.is_default_address = true
    handleOpen(false);
    handleLoading(true);
    props.addShippingAddress(data).then(async (resp) => {
      reset();
      handleLoading(false);
    });
  };

  if(isLoading) return "loading"
  if(isError) return "Error"

  return (
    <AccountLayout>
      <Title variant="h4" sx={{ paddingTop: 1, paddingBottom: 1 }}>
        Shipping Address
      </Title>
      <Content>Let’s get you started with DrugStoc</Content>

      {/* modal */}
      <Modal
        open={open}
        // dimmer="blurring"
        style={{ width: "40%" }}
        onClose={() => handleOpen(false)}
      >
        <Modal.Header>Add A new Shipping Address</Modal.Header>
        <Modal.Content>
          <Controller
            name="first_name"
            control={control2}
            rules={{
              required: {
                value: true,
                message: "Please your first name is required",
              },
            }}
            render={({ field }) => (
              <FormGroup>
                <TextField {...field} placeholder="Enter your first name" />
                {errors2.first_name && (
                  <Content
                    sx={{
                      marginTop: "8px",
                      fontSize: "10px",
                      color: "red",
                    }}
                  >
                    {errors2.first_name.message}
                  </Content>
                )}
              </FormGroup>
            )}
          />
          <Divider />
          <Controller
            name="last_name"
            control={control2}
            rules={{
              required: {
                value: true,
                message: "Please your last name is required",
              },
            }}
            render={({ field }) => (
              <FormGroup>
                <TextField {...field} placeholder="Enter your last name" />
                {errors2.last_name && (
                  <Content
                    sx={{
                      marginTop: "8px",
                      fontSize: "10px",
                      color: "red",
                    }}
                  >
                    {errors2.last_name.message}
                  </Content>
                )}
              </FormGroup>
            )}
          />
          <Divider />
          <Controller
            name="address_line1"
            control={control2}
            rules={{
              required: {
                value: true,
                message: "Please your address  is required",
              },
            }}
            render={({ field }) => (
              <FormGroup>
                <TextField {...field} placeholder="Enter your Address" />
                {errors2.address_line1 && (
                  <Content
                    sx={{
                      marginTop: "8px",
                      fontSize: "10px",
                      color: "red",
                    }}
                  >
                    {errors2.address_line1.message}
                  </Content>
                )}
              </FormGroup>
            )}
          />
          <Divider />
          <Controller
            name="address_line2"
            control={control2}
            render={({ field }) => (
              <FormGroup>
                <TextField
                  {...field}
                  placeholder="Enter your Additional Address"
                />
              </FormGroup>
            )}
          />
          <Divider />
          <Controller
            name="phone_number1"
            control={control2}
            rules={{
              required: {
                value: true,
                message: "Please your Phone  is required",
              },
            }}
            render={({ field }) => (
              <FormGroup>
                <TextField {...field} placeholder="Enter your Phone number" />
                {errors2.phone_number1 && (
                  <Content
                    sx={{
                      marginTop: "8px",
                      fontSize: "10px",
                      color: "red",
                    }}
                  >
                    {errors2.phone_number1.message}
                  </Content>
                )}
              </FormGroup>
            )}
          />
          <Divider />
          <Controller
            name="phone_number2"
            control={control2}
            render={({ field }) => (
              <FormGroup>
                <TextField
                  {...field}
                  placeholder="Enter your Additional Phone number"
                />
              </FormGroup>
            )}
          />
          <Divider />
          <Controller
            name="additional_information"
            control={control2}
            render={({ field }) => (
              <FormGroup>
                <TextField
                  {...field}
                  multiline
                  minRows={8}
                  maxRows={12}
                  placeholder="Enter your Additional Infomation number"
                />
              </FormGroup>
            )}
          />
          <Divider />
          <Controller
            name="region"
            control={control2}
            rules={{
              required: {
                value: true,
                message: "Please your Business Location is required",
              },
            }}
            render={({ field }) => (
              <FormGroup>
                <SelectField
                  {...field}
                  value={field.value || "Select your Business location" || ""}
                  defaultValue="Select your Business location"
                  input={<TextField />}
                  variant="standard"
                  inputProps={{
                    id: "uncontrolled-native",
                  }}
                >
                  <MenuItem disabled value="Select your Business location">
                    <div style={{ color: "grey" }}>
                      Select your Business location
                    </div>
                  </MenuItem>
                  {business_location.map((name) => (
                    <MenuItem key={name.key} value={name.value}>
                      {name.text}
                    </MenuItem>
                  ))}
                </SelectField>
              </FormGroup>
            )}
          />
        </Modal.Content>
        <Modal.Actions>
          <Stack spacing={2} direction="row" justifyContent="flex-end">
            <Button
              variant="outlined"
              sx={{ width: { xs: "50%", md: "20%" } }}
              onClick={() => handleOpen(false)}
            >
              Cancel
            </Button>
            <Button
              loading={loading}
              loadingIndicator={"Please wait ...."}
              variant="contained"
              sx={{ width: { xs: "50%", md: "20%" } }}
              onClick={handleSubmit2(onSubmit)}
            >
              Save
            </Button>
          </Stack>
        </Modal.Actions>
      </Modal>

      {/* page body */}
      <Box sx={{ marginTop: 5 }}>
        {!isLoading ? <Cards
          title="Your shipping Addresses"
          action={
            <Button
              disableElevation
              variant="contained"
              onClick={() => handleOpen(!open)}
              sx={{ height: 40 }}
              startIcon={<Add />}
            >
              Add New Address
            </Button>
          }
          empty={data | data.results.length > 0 ? false : true}
        >
          {data ? <Grid container spacing={3} sx={{ marginTop: 2 }}>
            {data.results.map((element, index) => (
              <Grid key={element.id} item md={6}>
                <ShippingItem token={data.token} data={element} />
              </Grid>
            ))}
          </Grid> : null}
        </Cards>: null}
      </Box>
    </AccountLayout>
  );
}

// ShippingAddress.getInitialProps = async (ctx) => {
//   const api = new Connect(BASE_URL);
//   const data = await api.getData({
//     path: `/shipping/shipping-address`,
//     token: ctx.req.cookies.accessToken,
//   });
//   return { data: data.data, token: ctx.req.cookies.accessToken  };
// };

// ShippingAddress.getInitialProps = getInitialProps(true, "/shipping/shipping-address")


const mapDispatchToProps = (dispatch) => {
  return {
    addShippingAddress: (payload) => dispatch(add_address(payload)),
  };
};

export default connect(null, mapDispatchToProps)(ShippingAddress);

import React, { useState } from "react";
import PropTypes from "prop-types";
import AccountLayout from "../../components/Layouts/account";
import { Content, Title } from "../../components/elements";
import Connect from "../../util/connect";
import { BASE_URL } from "../../util/resolveerror";
import { Grid } from "@mui/material";
import Product from "../../components/product";
import { useDataFetcher } from "../../util/hooks";

function Favorite(props) {
  const {data, isLoading, isError} = useDataFetcher({path: "/shopping/favorite/favorite-item", refreshInterval: true})
  // const [data, setData] = useState({
  //   count: props.data.count,
  //   next: props.data.next,
  //   previous: props.data.previous,
  //   results: props.data.results,
  // });

  // if(isLoading) return "loading"
  // if(isError) return "Error"

  return (
    <AccountLayout>
      <Title variant="h4" sx={{ paddingTop: 1, paddingBottom: 1 }}>
        My Favorite Products
      </Title>
      <Content>Let’s get you started with DrugStoc</Content>

      {!isLoading ? (
        <Grid sx={{ marginTop: 1 }} container spacing={3}>
          {data.results.map((element, index) => (
            <Grid item key={index} xs={12} md={4}>
              <Product
                id={element.product.id}
                title={element.product.name}
                image={element.product.image}
                description={element.product.desc}
                price={element.product.price}
                slug={element.product.slug}
                sku={element.product.SKU}
                quantity_in_cart={element.product.quantity_in_cart}
                in_cart={element.product.in_cart}
                quantity={element.product.quantity}
                category={element.product.category}
                in_fav={true}
                // setData={setData}
                is_favorite={element.product.is_favorite}
                manufacturer={element.product.manufacturer}
                composition={element.product.composition}
              />
            </Grid>
          ))}
        </Grid>
      ) : null}
    </AccountLayout>
  );
}

// Favorite.propTypes = {}

export default Favorite;

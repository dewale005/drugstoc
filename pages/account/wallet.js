import { Box, Button, Divider, MenuItem, Stack } from "@mui/material";
import { useEffect, useState } from "react";
import { connect } from "react-redux";
import {
  Content,
  FormGroup,
  Label,
  SelectField,
  TextField,
  Title,
} from "../../components/elements";
import AccountLayout from "../../components/Layouts/account";
import Transaction from "../../components/transaction";
import { accountBalance, createWallet } from "../../connect/transactions";
import { priceFormatDecimal } from "../../util/priceFormatter";
import { usePaystackPayment } from "react-paystack";
import { Modal } from "semantic-ui-react";
import { Controller, useForm } from "react-hook-form";
import { MonoButton, useMono } from "react-mono-js";
import { useDataFetcher } from "../../util/hooks";
import Cards from "../../components/sidebar/elements/cards";
import { wallet_currency } from "../../data/categories";

function Wallet(props) {
  const [amount, setAmount] = useState(null);
  const [open, handleOpen] = useState(false);
  const [loading, handleLoading] = useState(false);
  const [open_pos, handleOpenPos] = useState(false);

  const { data, isLoading, isError } = useDataFetcher({
    path: "/drugstocpay/wallet/account",
  });

  const config = {
    reference: new Date().getTime(),
    email: props.user.email,
    amount: amount * 100,
    publicKey: "pk_live_45ee06a67470f003a29bacea44b2117e90c0fec9",
  };

  const {
    control,
    formState: { errors },
    reset,
    handleSubmit,
  } = useForm({
    mode: "onBlur",
    defaultValues: {
      transaction_pin: "",
      currency: "",
    },
  });

  // const mono_config = {
  //   public_key: 'YOUR_CONNECT_PUBLIC_KEY',
  //   onClose: () => {},
  //   onSuccess: (response) => {
  //     console.log(response.code);
  //   },
  // };

  const handleMono = useMono({ public_key: "test_pk_DULREbkK47uYQsgALtpM" });

  // you can call this function anything
  const onSuccess = (reference) => {
    // Implementation for whatever you want to do with reference and after success call.
    console.log(reference);
  };

  // you can call this function anything
  const onClose = (e) => {
    // implementation for  whatever you want to do when the Paystack dialog closed.
    console.log("closed");
  };

  const onInvite = (data) => {
    handleLoading(true)
    props.createUserWallet(data).then((resp) => {
      reset({
        transaction_pin: "",
        currency: "",
      });
      handleLoading(false)
      console.log()
    })
  };

  useEffect(() => {
    props.getWalletBalance();
  }, []);

  const initializePayment = usePaystackPayment(config);

  if (isLoading) return "loading"
  if (isError) return "Error"

  return (
    <AccountLayout>
      <Title variant="h4" sx={{ paddingTop: 1, paddingBottom: 1 }}>
        DrugStocPay
      </Title>
      <Content>Let’s get you started with DrugStoc</Content>
      {data.results.length > 0 ? (
        <div>
          <Modal
            open={open}
            dimmer="blurring"
            style={{ width: "40%" }}
            onClose={() => handleOpen(false)}
          >
            <Modal.Header>Fund My Wallet</Modal.Header>
            <Modal.Content>
              <FormGroup>
                <TextField
                  value={amount}
                  onChange={(e) => setAmount(e.target.value)}
                  placeholder="Amount"
                />
              </FormGroup>
              <Divider />
            </Modal.Content>
            <Modal.Actions>
              <Stack
                spacing={2}
                height={35}
                direction="row"
                justifyContent="flex-end"
              >
                <Button
                  variant="outlined"
                  sx={{ width: { xs: "50%", md: "20%" } }}
                  onClick={() => handleOpen(false)}
                >
                  Cancel
                </Button>
                <Button
                  disableElevation
                  disabled={amount == null}
                  variant="contained"
                  sx={{ width: { xs: "50%", md: "30%" } }}
                  onClick={() => {
                    initializePayment(onSuccess, onClose);
                    setAmount(null);
                    handleOpen(false);
                  }}
                >
                  Fund Account
                </Button>
              </Stack>
            </Modal.Actions>
          </Modal>
          <Modal
            open={open_pos}
            dimmer="blurring"
            style={{ width: "40%" }}
            onClose={() => handleOpen(false)}
          >
            <Modal.Header>Request For POS Machine</Modal.Header>
            <Modal.Content>
              <FormGroup>
                <TextField placeholder="Full Name" />
              </FormGroup>
              <FormGroup>
                <TextField placeholder="Phone Number" />
              </FormGroup>
              <FormGroup>
                <TextField placeholder="Delivery Address" />
              </FormGroup>
              <FormGroup>
                <TextField placeholder="City" />
              </FormGroup>
              <FormGroup>
                <TextField placeholder="State" />
              </FormGroup>
              <Divider />
            </Modal.Content>
            <Modal.Actions>
              <Stack
                spacing={2}
                height={35}
                direction="row"
                justifyContent="flex-end"
              >
                <Button
                  variant="outlined"
                  sx={{ width: { xs: "50%", md: "20%" } }}
                  onClick={() => handleOpenPos(false)}
                >
                  Cancel
                </Button>
                <Button
                  disableElevation
                  variant="contained"
                  sx={{ width: { xs: "50%", md: "30%" } }}
                  onClick={() => handleOpenPos(false)}
                >
                  Make Request
                </Button>
              </Stack>
            </Modal.Actions>
          </Modal>
          <Box sx={{ marginTop: 5 }}>
            <Stack direction="row" spacing={3} justifyContent="space-between">
              <div>
                <Title variant="h5">Wallet Balance</Title>
                {props.userWallet.results.length > 0 ? (
                  <Title variant="h4">
                    {priceFormatDecimal(data.results[0].balance)}
                  </Title>
                ) : null}
                {data.results.length > 0 ? (
                  <Stack direction="row" spacing={3} sx={{ marginTop: 1 }}>
                    <Content variant="h6">
                      Available Balance:{" "}
                      {priceFormatDecimal(data.results[0].balance)}
                    </Content>
                    <Content variant="h6">
                      Ledgal Balance: {priceFormatDecimal(0)}
                    </Content>
                  </Stack>
                ) : null}
              </div>
              <Stack direction="row" spacing={3} height={35}>
                {/* <Button
                  disableElevation
                  variant="contained"
                  onClick={() => handleOpen(true)}
                >
                  Fund Wallet
                </Button>
                {/* <Button */}
                  {/* onClick={() =>
                    handleMono({
                      onClose: () => null,
                      onSuccess: (response) => {
                        console.log(response.code);
                      },
                    })
                  }
                  disableElevation
                  color="secondary"
                  variant="contained"
                >
                  Request for Credit
                </Button> */} 
                {/* <Button
                  disableElevation
                  onClick={() => handleOpenPos(true)}
                  color="secondary"
                  variant="contained"
                >
                  Request POS
                </Button> */}
              </Stack>
            </Stack>
            <Divider sx={{ marginTop: 2, marginBottom: 4 }} />
            {props.userWallet.results.length > 0 ? (
              <Transaction wallet={data.results[0].id} />
            ) : (
              <Content>Loading.....</Content>
            )}
          </Box>
        </div>
      ) : (
        <Box sx={{ marginTop: 5 }}>
          <Cards title="Create a new wallet">
          <Controller
              name="transaction_pin"
              control={control}
              rules={{
                required: {
                  value: true,
                  message: "Please your Transaction Pin is required",
                },
              }}
              render={({ field }) => (
                <FormGroup>
                  <Label>Transaction Pin*</Label>
                  <TextField {...field} maxLength={4} placeholder="Enter your Transaction Pin" type="password" />
                  {errors.transaction_pin && (
                    <Content
                      sx={{
                        marginTop: "8px",
                        fontSize: "10px",
                        color: "red",
                      }}
                    >
                      {errors.transaction_pin.message}
                    </Content>
                  )}
                </FormGroup>
              )}
            />
            <Controller
              name="currency"
              control={control}
              rules={{
                required: {
                  value: true,
                  message: "Please your Wallet Currency is required",
                },
              }}
              render={({ field }) => (
                <FormGroup>
                  <Label error={errors.currency && true}>
                    Currency*
                  </Label>
                  <SelectField
                    {...field}
                    error={errors.currency && true}
                    value={field.value || "Select your Wallet Currency" || ""}
                    defaultValue={
                      field.value || "Select your Wallet Currency" || ""
                    }
                    input={<TextField />}
                    variant="standard"
                    inputProps={{
                      id: "uncontrolled-native",
                    }}
                  >
                    <MenuItem disabled value="Select your Wallet Currency">
                      <div style={{ color: "grey" }}>
                        Select your Wallet Currency
                      </div>
                    </MenuItem>
                    {wallet_currency.map((name) => (
                      <MenuItem
                        key={name.key}
                        value={name.value}
                        // style={getStyles(name, personName, theme)}
                      >
                        {name.text}
                      </MenuItem>
                    ))}
                  </SelectField>
                  {errors.currency && (
                    <Content
                      sx={{
                        marginTop: "8px",
                        fontSize: "10px",
                        color: "red",
                      }}
                    >
                      {errors.currency.message}
                    </Content>
                  )}
                </FormGroup>
              )}
            />
            <Button
              loading={loading}
              loadingIndicator={"Please wait ...."}
              variant="contained"
              sx={{ width: { xs: "50%", md: "20%" } }}
              onClick={handleSubmit(onInvite)}
            >
              Create Wallet
            </Button>
          </Cards>
        </Box>
      )}
    </AccountLayout>
  );
}

const mapStateToProps = (state) => {
  return {
    userWallet: state.transaction.wallet,
    user: state.user.user,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getWalletBalance: () => dispatch(accountBalance()),
    createUserWallet: (payload) => dispatch(createWallet(payload))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Wallet);

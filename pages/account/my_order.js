import React from 'react'
import PropTypes from 'prop-types'
import AccountLayout from '../../components/Layouts/account'
import { Content, Title } from '../../components/elements'

function MyOrder(props) {
  return (
    <AccountLayout>
    <Title variant="h4" sx={{ paddingTop: 1, paddingBottom: 1 }}>
      Shipping Address
    </Title>
    <Content>Let’s get you started with DrugStoc</Content>

  </AccountLayout>
  )
}

MyOrder.propTypes = {}

export default MyOrder

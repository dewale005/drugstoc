import React from 'react'
import { Content, Title } from '../../components/elements'
import AccountLayout from '../../components/Layouts/account'

function Reorder(Props) {
  return (
    <AccountLayout>
    <Title variant="h4" sx={{ paddingTop: 1, paddingBottom: 1 }}>
      Reorder Items
    </Title>
    <Content>Let’s get you started with DrugStoc</Content>

  </AccountLayout>
  )
}

export default Reorder
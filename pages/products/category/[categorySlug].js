import { Container, Divider, Grid, Paper } from "@mui/material";
import { useRouter } from "next/router";
import PropTypes from "prop-types";
import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import { Title } from "../../../components/elements";
import ProductsListLayout from "../../../components/Layouts/product";
import Navbar from "../../../components/Navbar/Navbar";
import Products from "../../../components/product";
import { category_products } from "../../../connect/products";
import { products } from "../../../data/products";
import Connect from "../../../util/connect";
import { useDataFetcher } from "../../../util/hooks";
import { BASE_URL } from "../../../util/resolveerror";

export const ProductListCategory = (props) => {

  const {data, isLoading, isError} = useDataFetcher({path: `/inventory/products/category/${props.query}`,})

  // const [data, setData] = useState(props.data.results)
  // const [next, setNext] = useState(props.data.next)
  // const [prev, setPrev] = useState(props.data.prev)
  // const [count, setCount] = useState(props.data.count)

  // useEffect(() => {
  //   props.getCategoryProduct(props.query);
  // }, []);

  if(isLoading) return "loading"
  if(isError) return "Error"

  console.log(data, props)

  return (
    <ProductsListLayout>
      {props.productsResult ? (
        <Grid sx={{ marginTop: 1 }} container spacing={3}>
          {data.results.map((element, index) => (
            <Grid item key={element.id} xs={12} md={3}>
              <Products
                id={element.id}
                title={element.name}
                image={element.image}
                description={element.desc}
                price={element.price}
                slug={element.slug}
                sku={element.SKU}
                quantity_in_cart={element.quantity_in_cart}
                in_cart={element.in_cart}
                quantity={element.quantity}
                category={element.category}
                is_favorite={element.is_favorite}
                manufacturer={element.manufacturer}
                composition={element.composition}
              />
            </Grid>
          ))}
        </Grid>
      ) : null}
    </ProductsListLayout>
  );
};

ProductListCategory.propTypes = {
  //   second: PropTypes.third,
};

ProductListCategory.getInitialProps = async ({ query, req }) => {
  return { query: query.categorySlug,  };
};

const mapStateToProps = (state) => {
  return {
    productsResult: state.products.category_products,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    getCategoryProduct: (slug) => dispatch(category_products(slug)),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProductListCategory);

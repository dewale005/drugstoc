import { Add, Remove } from "@mui/icons-material";
import { Box, Container, Divider, Grid, Stack } from "@mui/material";
import { useRouter } from "next/router";
import PropTypes from "prop-types";
import React, { useState } from "react";
import { connect } from "react-redux";
import { Content, Title } from "../../components/elements";
import Navbar from "../../components/Navbar/Navbar";
import { AddToCartButton, IconButton } from "../../components/product/styles";
import {
  addToCart,
  addToFav,
  deleteCartItem,
  delToFav,
  getCartItems,
  updateCartItem,
} from "../../connect/shopping";
import Connect from "../../util/connect";
import { useDataFetcher } from "../../util/hooks";
import { priceFormat } from "../../util/priceFormatter";
import { BASE_URL } from "../../util/resolveerror";

export const ProductsDetails = (props) => {
  const { data, isLoading, isError } = useDataFetcher({
    path: "/inventory/products/" + props.slug,
  });
  const [quantity, setQuantity] = useState(props.data.quantity_in_cart || 1);
  const [loading_decrease, setLoadingDecrease] = useState(false);
  const [loading_increase, setLoadingIncrease] = useState(false);
  const [inFav, setFav] = useState(props.is_favorite);
  const [inCart, setCart] = useState(props.in_cart);
  const [loading_cart, setLoadingCart] = useState(false);
  const [loading_fav, setLoadingFav] = useState(false);
  const [loading_del, setLoadingDelete] = useState(false);

  function add_to_cart() {
    let data = {
      "quantity": 1,
      "product_id": props.data.id
    }
    setLoadingCart(true);
    props.addItemToCart(data).then((resp) => {
      if (resp.success) {
        setLoadingCart(false);
        props.loadCartItem()
        setCart(true);
      } else {
        setLoadingCart(false);
      }
    })
  }

  function add_to_fav() {
    let data = {
      "product_id": props.id
    }
    setLoadingFav(true);
    props.addItemToFav(data).then((resp) => {
      if (resp.success) {
        setLoadingFav(false);
        setFav(true);
      } else {
        setLoadingFav(false);
      }
    })
  }

  function delete_fav() {
    setLoadingDelete(true);
    props.deleteItemToFav(props.id).then(resp => {
      if (resp.success) {
        setLoadingDelete(false);
        props.loadCartItem()
      } else {
        setLoadingDelete(false);
      }
    })
  }

  function increase_item() {
    setLoadingIncrease(true)
    let data = {
      "quantity": quantity+1,
      "product_id": props.data.id
    }
    props.updateItemInCart(data, props.data.id).then(resp => {
      if (resp.success) {
        setQuantity(quantity+=1)
        setLoadingIncrease(false)
      } else {
        setLoadingIncrease(false)
      }
    })
  }

  function decrease_item() {
    setLoadingDecrease(true)
    let data = {
      "quantity": quantity-1,
      "product_id": props.data.id
    }
    if (quantity == 1) {
      props.deleteItemInCart(props.data.id).then(resp => {
        if (resp.success) {
          setQuantity(1)
          setLoadingDecrease(false)
          props.loadCartItem();
          setLoadingCart(false);
          setCart(false);
        } else {
          setLoadingDecrease(false)
        }
      })
    } else {
      props.updateItemInCart(data, props.data.id).then(resp => {
        if (resp.success) {
          let qunt = quantity-=1
          setQuantity(qunt)
          setLoadingDecrease(false)
        } else {
          setLoadingDecrease(false)
        }
      })
    }
  }

  if (isLoading) {
    return "Loading...."
  }

  return (
    <div>
      <Navbar />
      <Box sx={{ marginTop: 3 }}>
        <Container fixed>
          {props.data ? (
            <>
              <Grid container spacing={2}>
                <Grid item md={6} xs={12}>
                  <Box
                    component="img"
                    loading="lazy"
                    sx={{
                      width: "100%",
                      height: 430,
                      cursor: "pointer",
                      objectFit: "contain",
                    }}
                    alt="product image"
                    src={props.data.image}
                  />
                </Grid>
                <Grid item md={6} xs={12}>
                  <Title
                    variant="h4"
                    sx={{
                      lineHeight: 1.4,
                    }}
                  >
                    {props.data.name}
                  </Title>
                  <Content
                    sx={{
                      fontSize: 12,
                      textOverflow: "ellipsis",
                      overflow: "hidden",
                      whiteSpace: "nowrap",
                    }}
                  >
                    <Title>SKU: {props.data.SKU}</Title>
                  </Content>
                  <Content
                    sx={{
                      fontSize: 12,
                      textOverflow: "ellipsis",
                      overflow: "hidden",
                      whiteSpace: "nowrap",
                    }}
                  >
                    <Title>MANUFACTURER: {props.data.manufacturer}</Title>
                  </Content>
                  <Content
                    sx={{
                      fontSize: 12,
                      textOverflow: "ellipsis",
                      overflow: "hidden",
                      whiteSpace: "nowrap",
                    }}
                  >
                    DESCRIPTION: {props.data.desc}
                  </Content>
                  <Divider sx={{ marginTop: 2.5, marginBottom: 2.5 }} />
                  <Title
                    variant="h5"
                    sx={{
                      filter: data.price ? "blur(0px)" : "blur(5px)",
                    }}
                  >
                    {data.price
                      ? priceFormat(data.price)
                      : "PRICE NOT AVAILABLE"}
                  </Title>
                  <Divider sx={{ marginTop: 2.5, marginBottom: 2.5 }} />
                  {!inCart ? (
                    <AddToCartButton
                      variant="contained"
                      disabled={props.quantity == 0}
                      onClick={add_to_cart}
                      loading={loading_cart}
                      sx={{ width: "100%", fontSize: 12, fontWeight: "900" }}
                      disableElevation
                    >
                      {props.quantity == 0 ? "Out of Stock" : "Add to cart"}
                    </AddToCartButton>
                  ) : (
                    <Stack
                      direction="row"
                      justifyContent="space-between"
                      width="100%"
                      spacing={3}
                    >
                      <IconButton
                        onClick={decrease_item}
                        loading={loading_decrease}
                        variant="contained"
                      >
                        <Remove />
                      </IconButton>
                      <Title>{quantity}</Title>
                      <IconButton
                        onClick={increase_item}
                        loading={loading_increase}
                        variant="contained"
                      >
                        <Add />
                      </IconButton>
                    </Stack>
                  )}
                </Grid>
              </Grid>
              <Divider sx={{ marginTop: 2.5, marginBottom: 2.5 }} />
              <Title
                variant="h6"
                sx={{
                  lineHeight: 1.4,
                }}
              >
                ALTERNATIVE PRODUCTS
              </Title>
            </>
          ) : null}
        </Container>
      </Box>
    </div>
  );
};

ProductsDetails.propTypes = {};

ProductsDetails.getInitialProps = async (ctx) => {
  const api = new Connect(BASE_URL);
  const data = await api.getData({
    path: `/inventory/products/${ctx.query.productsSlug}`,
  });
  return { data: data.data, slug: ctx.query.productsSlug };
};

const mapStateToProps = (state) => ({});

const mapDispatchToProps = (dispatch) => {
  return {
    addItemToCart: (data) => dispatch(addToCart(data)),
    addItemToFav: (data) => dispatch(addToFav(data)),
    deleteItemToFav: (id) => dispatch(delToFav(id)),
    updateItemInCart: (data, id) => dispatch(updateCartItem(data, id)),
    deleteItemInCart: (id) => dispatch(deleteCartItem(id)),
    loadCartItem: () => dispatch(getCartItems()),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(ProductsDetails);

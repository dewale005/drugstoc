import { Router } from "next/router";
import { NextResponse } from "next/server";
import Connect from "./connect";

export const getError = (response) => {
  let err = Object.entries(response);
  let errResponse = [];
  for (let i = 0; i < err.length; i++) {
    let obj = {
      type: err[i][0],
      message: err[i][1][0],
    };
    errResponse.push(obj);
  }
  return errResponse;
};

// export const BASE_URL = "http://localhost:8000/api/v2"
export const BASE_URL = "https://staging.drugstoc.com/api/v2";

export const getInitialProps =
  (isProtectedRoute, path) =>
  async ({ req, res }) => {
    // console.log(window.Cookies.get("accessToken"))
    const api = new Connect(BASE_URL);
    const auth = req && req.cookies.accessToken ? req.cookies.accessToken : null;
    const currentPath = req ? req.url : window.location.pathname;
    if (isProtectedRoute && auth === null && currentPath !== "/login") {
      if (res) {
        res.writeHead(302, { Location: '/login' }).end();
        res.finished = true;
        return { loading: false, data: null };
      }
      return {loading: false, data: null };
    }

    if (res) {
      const data = await api.getData({
        path: path,
        token: auth,
      });
      // console.log(data.data, res)
      return { loading: false, data: data.data, token: auth};
    }

    return {loading: false, data: {count: 0,next: null, previous: null,results: []} };
  };

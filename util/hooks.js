import { BASE_URL } from "./resolveerror";
import useSWR from "swr";
import axios from "axios";
import Cookies from "js-cookie";
import { useEffect } from "react";
import Router from "next/router";

const fetcher = (url) => {
  let token = Cookies.get("accessToken");
  return axios
    .get(url, {
      headers: {
        "Content-Type": "application/json",
        Authorization: token ? `Token ${token}` : null,
      },
    })
    .then((res) => {
      res.data.token = token;
      return res.data;
    });
};

export function useDataFetcher({ path, refreshInterval = false }) {
  const { data, error } = useSWR(`${BASE_URL}${path}`, fetcher, {
    refreshInterval: refreshInterval ? 500 : 0,
  });

  //   console.log(data, error?.response);

  //   useEffect(() => {
  // if no redirect needed, just return (example: already on /dashboard)
  // if user data not yet there (fetch in progress, logged in or not) then don't do anything yet
  // if (!user) return

  if (
    // If redirectTo is set, redirect if the user was not found.
    error?.response.status === 401 ||
    // If redirectIfFound is also set, redirect if the user was found
    error?.response.status === 403
  ) {
    Cookies.remove("accessToken");
    Router.push("/login");
    return {
        data: null,
        isLoading: true,
        isError: false,
      };
  } else {
    console.log(data, error?.response, !error && !data);

    return {
      data: data,
      isLoading: !error && !data,
      isError: error,
    };
  }
  //   });
}

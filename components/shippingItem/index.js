import React, { useState } from "react";
import PropTypes from "prop-types";
import Cards from "../sidebar/elements/cards";
import { Box, Button, Checkbox, colors, Divider, FormControlLabel, Stack } from "@mui/material";
import { Delete, Edit } from "@mui/icons-material";
import {
  Content,
  FormGroup,
  MenuItem,
  SelectField,
  TextField,
} from "../elements";
import { Modal } from "semantic-ui-react";
import { Controller, useForm } from "react-hook-form";
import { business_location } from "../../data/categories";
import Connect from "../../util/connect";
import { BASE_URL } from "../../util/resolveerror";
import { connect } from "react-redux";
import { delete_address, update_address } from "../../connect/shipping";

function ShippingItem(props) {
  const [open, handleOpen] = useState(false);
  const [loading, handleLoading] = useState(false);

  const {
    control: control2,
    reset,
    formState: { errors: errors2 },
    handleSubmit: handleSubmit2,
  } = useForm({
    defaultValues: {
      first_name: props.data.first_name,
      last_name: props.data.last_name,
      address_line1: props.data.address_line1,
      address_line2: props.data.address_line2,
      phone_number1: props.data.phone_number1,
      phone_number2: props.data.phone_number2,
      additional_information: props.data.additional_information,
      region: props.data.region,
      is_default_address: props.data.is_default,
    },
  });


  const onSubmit = (data) => {
    // data.
    handleOpen(false);
    handleLoading(true);
    console.log(data);
    props.updateShippingAddress(data, props.data.id).then(async (resp) => {
      const api = new Connect(BASE_URL);
      const data = await api.getData({
        path: `/shipping/shipping-address`,
        token: props.token,
      });
      // props.setData(data.data);
      handleLoading(false);
    });
  };

  const onDelete = () => {
    // data.
    handleOpen(false);
    handleLoading(true);
    props.deleteShippingAddress(props.data.id).then(async (resp) => {
      const api = new Connect(BASE_URL);
      const data = await api.getData({
        path: `/shipping/shipping-address`,
        token: props.token,
      });
      handleLoading(false);
    });
  };

  return (
    <Cards
      title=""
      action={
        <Stack direction="row" sx={{ marginBottom: 1 }} spacing={2}>
          <Button
            variant="outlined"
            onClick={() => handleOpen(true)}
            startIcon={<Edit />}
          >
            Edit
          </Button>
          <Button
            variant="outlined"
            onClick={onDelete}
            startIcon={<Delete />}
            color="error"
          >
            Delete
          </Button>
        </Stack>
      }
    >
      {/* modal */}
      <Modal
        open={open}
        // dimmer="blurring"
        style={{ width: "40%" }}
        onClose={() => handleOpen(false)}
      >
        <Modal.Header>Update Shipping Address</Modal.Header>
        <Modal.Content>
          <Controller
            name="is_default_address"
            control={control2}
            render={({ field }) => (
              <FormGroup>
                <FormControlLabel
                  {...field}
                  // defaultValue={props.data.is_default}
                  control={<Checkbox defaultChecked={props.data.is_default} />}
                  label="Set As Default Address"
                />
              </FormGroup>
            )}
          />
          <Divider />
          <Controller
            name="first_name"
            control={control2}
            rules={{
              required: {
                value: true,
                message: "Please your first name is required",
              },
            }}
            render={({ field }) => (
              <FormGroup>
                <TextField {...field} placeholder="Enter your first name" />
                {errors2.first_name && (
                  <Content
                    sx={{
                      marginTop: "8px",
                      fontSize: "10px",
                      color: "red",
                    }}
                  >
                    {errors2.first_name.message}
                  </Content>
                )}
              </FormGroup>
            )}
          />
          <Divider />
          <Controller
            name="last_name"
            control={control2}
            rules={{
              required: {
                value: true,
                message: "Please your last name is required",
              },
            }}
            render={({ field }) => (
              <FormGroup>
                <TextField {...field} placeholder="Enter your last name" />
                {errors2.last_name && (
                  <Content
                    sx={{
                      marginTop: "8px",
                      fontSize: "10px",
                      color: "red",
                    }}
                  >
                    {errors2.last_name.message}
                  </Content>
                )}
              </FormGroup>
            )}
          />
          <Divider />
          <Controller
            name="address_line1"
            control={control2}
            rules={{
              required: {
                value: true,
                message: "Please your address  is required",
              },
            }}
            render={({ field }) => (
              <FormGroup>
                <TextField {...field} placeholder="Enter your Address" />
                {errors2.address_line1 && (
                  <Content
                    sx={{
                      marginTop: "8px",
                      fontSize: "10px",
                      color: "red",
                    }}
                  >
                    {errors2.address_line1.message}
                  </Content>
                )}
              </FormGroup>
            )}
          />
          <Divider />
          <Controller
            name="address_line2"
            control={control2}
            render={({ field }) => (
              <FormGroup>
                <TextField
                  {...field}
                  placeholder="Enter your Additional Address"
                />
              </FormGroup>
            )}
          />
          <Divider />
          <Controller
            name="phone_number1"
            control={control2}
            rules={{
              required: {
                value: true,
                message: "Please your Phone  is required",
              },
            }}
            render={({ field }) => (
              <FormGroup>
                <TextField {...field} placeholder="Enter your Phone number" />
                {errors2.phone_number1 && (
                  <Content
                    sx={{
                      marginTop: "8px",
                      fontSize: "10px",
                      color: "red",
                    }}
                  >
                    {errors2.phone_number1.message}
                  </Content>
                )}
              </FormGroup>
            )}
          />
          <Divider />
          <Controller
            name="phone_number2"
            control={control2}
            render={({ field }) => (
              <FormGroup>
                <TextField
                  {...field}
                  placeholder="Enter your Additional Phone number"
                />
              </FormGroup>
            )}
          />
          <Divider />
          <Controller
            name="additional_information"
            control={control2}
            render={({ field }) => (
              <FormGroup>
                <TextField
                  {...field}
                  multiline
                  minRows={8}
                  maxRows={12}
                  placeholder="Enter your Additional Infomation number"
                />
              </FormGroup>
            )}
          />
          <Divider />
          <Controller
            name="region"
            control={control2}
            rules={{
              required: {
                value: true,
                message: "Please your Business Location is required",
              },
            }}
            render={({ field }) => (
              <FormGroup>
                <SelectField
                  {...field}
                  value={field.value || "Select your Business location" || ""}
                  defaultValue="Select your Business location"
                  input={<TextField />}
                  variant="standard"
                  inputProps={{
                    id: "uncontrolled-native",
                  }}
                >
                  <MenuItem disabled value="Select your Business location">
                    <div style={{ color: "grey" }}>
                      Select your Business location
                    </div>
                  </MenuItem>
                  {business_location.map((name) => (
                    <MenuItem key={name.key} value={name.value}>
                      {name.text}
                    </MenuItem>
                  ))}
                </SelectField>
              </FormGroup>
            )}
          />
        </Modal.Content>
        <Modal.Actions>
          <Stack spacing={2} direction="row" justifyContent="flex-end">
            <Button
              variant="outlined"
              sx={{ width: { xs: "50%", md: "20%" } }}
              onClick={() => handleOpen(false)}
            >
              Cancel
            </Button>
            <Button
              loading={loading}
              loadingIndicator={"Please wait ...."}
              variant="contained"
              sx={{ width: { xs: "50%", md: "20%" } }}
              onClick={handleSubmit2(onSubmit)}
            >
              Save
            </Button>
          </Stack>
        </Modal.Actions>
      </Modal>

      <Content sx={{ marginTop: 2 }}>
        {props.data.first_name} {props.data.last_name}
      </Content>
      <Content sx={{ marginTop: 1, fontSize: 12 }}>
        {props.data.address_line1} {props.data.address_line2}{" "}
        {props.data.region}
      </Content>
      <Stack
        direction="row"
        sx={{ marginTop: 1 }}
        justifyContent="space-between"
      >
        <Box>
          <Content sx={{ fontSize: 12 }}>{props.data.phone_number1}</Content>
          <Content sx={{ fontSize: 12 }}>{props.data.phone_number2}</Content>
        </Box>
        <Content sx={{ fontSize: 12, color: "green", fontWeight: "500" }}>
          {props.data.is_default ? "Default Address" : ""}
        </Content>
      </Stack>
    </Cards>
  );
}

const mapDispatchToProps = (dispatch) => {
  return {
    updateShippingAddress: (payload, id) =>
      dispatch(update_address(payload, id)),
    deleteShippingAddress: (id) => dispatch(delete_address(id)),
  };
};

export default connect(null, mapDispatchToProps)(ShippingItem);

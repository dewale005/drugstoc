import { shippingAddressAction } from "../../store/actions/shipping";
import Connect from "../../util/connect";
import { BASE_URL } from "../../util/resolveerror";

const connect = new Connect(BASE_URL)


export const shipping_address = () => connect.get({
    path: "/shipping/shipping-address",
    action: shippingAddressAction
})

export const add_address = (payload) => connect.post({
    path: "/shipping/shipping-address",
    payload: payload,
})

export const update_address = (payload, id) => connect.patch({
    path: "/shipping/shipping-address/" + id,
    payload: payload,
})

export const delete_address = (id) => connect.delete({
    path: "/shipping/shipping-address/" + id,
})